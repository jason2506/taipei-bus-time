import { useNavigate } from '@solidjs/router'
import { fileOpen, fileSave } from 'browser-fs-access'
import type { JSX } from 'solid-js'

import { Layout, LayoutContent, LayoutHeader } from '@/components/Layout'
import { List, ListItem } from '@/components/List'
import { Snackbar, SnackbarContent } from '@/components/Snackbar'
import { TopAppBar, TopAppBarHeadline, TopAppBarNavigation } from '@/components/TopAppBar'
import { createQueue } from '@/primitives'
import { usePreferenceService } from '@/services'

import styles from './style.module.scss'

const dtf = new Intl.DateTimeFormat('en-GB', {
  dateStyle: 'short',
  timeStyle: 'medium',
  timeZone: 'Asia/Taipei',
})

function formatDateTime(date: number | Date): string {
  const parts = dtf.formatToParts(date)
  const partRecord: { [K in (typeof parts)[number]['type']]?: string } = {}
  for (const { type, value } of parts) {
    partRecord[type] = value
  }

  const { year, month, day, hour, minute, second } = partRecord
  return `${year}${month}${day}-${hour}${minute}${second}`
}

export default function ImportExportPage(): JSX.Element {
  const preferences = usePreferenceService()

  const [snackbarText, { push: pushSnackbarText, pop: popSnackbarText }] = createQueue<string>()
  async function importPreferenceAsync(): Promise<void> {
    try {
      const blob = await fileOpen({
        mimeTypes: ['application/json'],
        extensions: ['.json'],
      })
      const json = await new Response(blob).text()
      preferences.import(json)
      pushSnackbarText('已匯入偏好設定。')
    } catch {
      pushSnackbarText('偏好設定匯入失敗。')
    }
  }

  function importPreference(): void {
    void importPreferenceAsync()
  }

  function exportPreference(): void {
    const json = preferences.export()
    const blob = new Blob([json], { type: 'application/json' })
    void fileSave(blob, {
      fileName: `taipei-bus-time-${formatDateTime(Date.now())}.json`,
      extensions: ['.json'],
    })
  }

  const navigate = useNavigate()
  return (
    <Layout>
      <LayoutHeader title="匯入／匯出">
        <TopAppBar>
          <TopAppBarNavigation
            type="button"
            aria-label="返回"
            icon="arrow_back"
            onClick={[navigate, -1]}
          />
          <TopAppBarHeadline>匯入／匯出</TopAppBarHeadline>
        </TopAppBar>
      </LayoutHeader>
      <LayoutContent>
        <List>
          <ListItem type="button" onClick={importPreference}>
            匯入檔案
          </ListItem>
          <ListItem type="button" onClick={exportPreference}>
            匯出檔案
          </ListItem>
        </List>
        <Snackbar
          open={snackbarText() !== undefined}
          class={styles['snackbar']}
          onClosed={popSnackbarText}
        >
          <SnackbarContent>{snackbarText()}</SnackbarContent>
        </Snackbar>
      </LayoutContent>
    </Layout>
  )
}
