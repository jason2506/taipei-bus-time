import type { MdDialog } from '@material/web/dialog/dialog'
import type { MdOutlinedTextField } from '@material/web/textfield/outlined-text-field'
import { Entries } from '@solid-primitives/keyed'
import { useNavigate } from '@solidjs/router'
import type { JSX } from 'solid-js'
import { createMemo, createSignal, Show } from 'solid-js'

import {
  Dialog,
  DialogAction,
  DialogActions,
  DialogContent,
  DialogHeadline,
} from '@/components/Dialog'
import { ItemEnd, ItemHeadline } from '@/components/Item'
import { Layout, LayoutContent, LayoutHeader } from '@/components/Layout'
import { SortableList, SortableListDragHandle, SortableListItem } from '@/components/SortableList'
import { TextField } from '@/components/TextField'
import { TopAppBar, TopAppBarHeadline, TopAppBarNavigation } from '@/components/TopAppBar'
import { useBusStopBookmarkService } from '@/services'
import type { JSXEvent } from '@/utils/events'

import styles from './style.module.scss'

export default function CategoryListPage(): JSX.Element {
  const bookmarkService = useBusStopBookmarkService()
  const categories = createMemo(() => bookmarkService.getCategories())

  const [editingCategoryId, setEditingCategoryId] = createSignal<string>()
  const [editingCategoryName, setEditingCategoryName] = createSignal('')
  function startEditCategory(categoryId: string): void {
    setEditingCategoryId(categoryId)
    setEditingCategoryName(categories()[categoryId] ?? '')
  }

  function endEditCategory(event: JSXEvent<MdDialog>): void {
    const el = event.currentTarget
    const categoryId = editingCategoryId()
    if (categoryId !== undefined && el.returnValue === 'submit') {
      bookmarkService.renameCategory(categoryId, editingCategoryName())
    }

    el.returnValue = ''
    setEditingCategoryId()
    setEditingCategoryName('')
  }

  function updateCategoryName(event: JSXEvent<MdOutlinedTextField>): void {
    setEditingCategoryName(event.currentTarget.value)
  }

  let containerEl!: HTMLElement
  const navigate = useNavigate()
  return (
    <Layout>
      <LayoutHeader title="管理收藏分類">
        <TopAppBar>
          <TopAppBarNavigation
            type="button"
            aria-label="返回"
            icon="arrow_back"
            onClick={[navigate, -1]}
          />
          <TopAppBarHeadline>管理收藏分類</TopAppBarHeadline>
        </TopAppBar>
      </LayoutHeader>
      <LayoutContent
        ref={(el) => {
          containerEl = el
        }}
      >
        <Show
          when={Object.keys(categories()).length > 0}
          fallback={
            <section class={styles['fallback']}>
              <h1>尚無收藏分類</h1>
              <p>當您將站牌加入收藏後，便能在此管理與其一同建立的分類。</p>
            </section>
          }
        >
          <SortableList
            container={containerEl}
            onDropped={(from, to) => {
              bookmarkService.reorderCategory(from, to)
            }}
          >
            <Entries of={categories()}>
              {(id, name, index) => (
                <SortableListItem
                  type="button"
                  aria-expanded={editingCategoryId() === id}
                  aria-haspopup="dialog"
                  onClick={[startEditCategory, id]}
                >
                  <ItemHeadline>{name()}</ItemHeadline>
                  <ItemEnd as={SortableListDragHandle} index={index()} />
                </SortableListItem>
              )}
            </Entries>
          </SortableList>
        </Show>
      </LayoutContent>
      <Dialog open={editingCategoryId() !== undefined} on:closed={endEditCategory}>
        <DialogHeadline>編輯收藏分類</DialogHeadline>
        <DialogContent>
          <TextField
            aria-label="收藏分類名稱"
            variant="outlined"
            placeholder="收藏分類名稱"
            onInput={updateCategoryName}
            value={editingCategoryName()}
          />
        </DialogContent>
        <DialogActions>
          <DialogAction>取消</DialogAction>
          <DialogAction value="submit" disabled={editingCategoryName() === ''}>
            確定
          </DialogAction>
        </DialogActions>
      </Dialog>
    </Layout>
  )
}
