import { Entries, Key } from '@solid-primitives/keyed'
import { ReactiveSet } from '@solid-primitives/set'
import { useLocation, useNavigate } from '@solidjs/router'
import classNames from 'classnames'
import type { JSX } from 'solid-js'
import { createEffect, createMemo, createSignal, For, Show } from 'solid-js'

import { BookmarkDialog } from '@/components/BookmarkDialog'
import { BusArrivalTime } from '@/components/BusArrivalTime'
import { Fab } from '@/components/Fab'
import { Icon } from '@/components/Icon'
import { ItemEnd, ItemHeadline, ItemStart, ItemSupportingText } from '@/components/Item'
import { Layout, LayoutContent, LayoutHeader } from '@/components/Layout'
import { Menu, MenuItem } from '@/components/Menu'
import { Snackbar, SnackbarContent } from '@/components/Snackbar'
import { SortableList, SortableListDragHandle, SortableListItem } from '@/components/SortableList'
import { Tab, Tabs } from '@/components/Tabs'
import {
  TopAppBar,
  TopAppBarAction,
  TopAppBarActions,
  TopAppBarHeadline,
  TopAppBarNavigation,
} from '@/components/TopAppBar'
import { createQueue } from '@/primitives'
import { useBusInfoService, useBusStopBookmarkService } from '@/services'

import styles from './style.module.scss'

export default function BookmarkListPage(): JSX.Element {
  const bookmarkService = useBusStopBookmarkService()
  const bookmarks = bookmarkService.getBookmarks()

  const navigate = useNavigate()
  const location = useLocation()

  const categories = createMemo(() => bookmarkService.getCategories())
  const selectedCategoryId = createMemo(() => {
    const category = location.hash.substring(1)
    return category in categories() ? category : Object.keys(categories())[0] ?? ''
  })
  function onSelectCategory(category: string): void {
    navigate(`/#${category}`, { replace: true })
  }

  const busInfoService = useBusInfoService()
  const estimateTimes = busInfoService.getEstimateTimesByStops(() =>
    bookmarkService.getStopIds().values(),
  )

  const selectedStopIds = new ReactiveSet<string>()

  let selectStopIdTimeout: number | undefined
  let isCurrentBookmarkSelected = false
  function onStartPressBookmark(stopId: string): void {
    isCurrentBookmarkSelected = selectedStopIds.has(stopId)
    selectStopIdTimeout = setTimeout(() => {
      selectedStopIds.add(stopId)
    }, 500)
  }

  function onEndPressBookmark(): void {
    clearTimeout(selectStopIdTimeout)
  }

  function onClickBookmark(stopId: string, event: Event): void {
    if (selectedStopIds.size > 0) {
      event.preventDefault()
      if (!selectedStopIds.has(stopId)) {
        selectedStopIds.add(stopId)
      } else if (isCurrentBookmarkSelected) {
        selectedStopIds.delete(stopId)
      }
    }
  }

  function clearSelectedBookmarks(): void {
    selectedStopIds.clear()
  }

  const [categoryStatus, setCategoryStatus] =
    createSignal<Readonly<Record<string, readonly [string, boolean | null]>>>()
  function openBookmarkDialog(): void {
    const categoryStatus: Record<string, readonly [string, boolean | null]> = {}
    const categories = bookmarkService.getCategories()
    for (const [categoryId, categoryName] of Object.entries(categories)) {
      let checked: boolean | null | undefined
      for (const stopId of selectedStopIds) {
        const hasCategory = bookmarkService.getCategoriesByStop(stopId).includes(categoryId)
        checked = checked === undefined ? hasCategory : checked === hasCategory ? checked : null
      }

      categoryStatus[categoryId] = [categoryName, checked === undefined ? false : checked]
    }
    setCategoryStatus(categoryStatus)
  }

  const [snackbarText, { push: pushSnackbarText, pop: popSnackbarText }] = createQueue<string>()
  function onCloseBookmarkDialog(categories?: {
    readonly updated: Readonly<Record<string, boolean>>
    readonly created: readonly string[]
  }): void {
    if (categories !== undefined) {
      bookmarkService.updateBookmarks(
        Array.from(selectedStopIds),
        categories.updated,
        categories.created,
      )
      pushSnackbarText(`已更新 ${selectedStopIds.size} 個站牌的收藏分類。`)
      clearSelectedBookmarks()
    }
    setCategoryStatus()
  }

  const [menuAnchorEl, setMenuAnchorEl] = createSignal<HTMLElement>()
  const [isMenuOpen, setMenuOpen] = createSignal(false)
  createEffect(() => {
    if (!bookmarks.isLoading() && bookmarks.error() !== undefined) {
      pushSnackbarText('無法取得公車路線和站牌資訊。')
    } else if (!estimateTimes.isLoading() && estimateTimes.error() !== undefined) {
      pushSnackbarText('無法取得公車動態資訊。')
    }
  })

  let containerEl: HTMLElement
  return (
    <Layout>
      <LayoutHeader
        loadingInfo={
          bookmarks.isLoading() || estimateTimes.isLoading() ? '正在載入收藏站牌資訊' : undefined
        }
      >
        <TopAppBar>
          <Show when={selectedStopIds.size > 0}>
            <TopAppBarNavigation icon="close" onClick={clearSelectedBookmarks} />
          </Show>
          <TopAppBarHeadline>
            {selectedStopIds.size === 0 ? '已收藏站牌' : `已選取 ${selectedStopIds.size}`}
          </TopAppBarHeadline>
          <TopAppBarActions>
            <Show
              when={selectedStopIds.size === 0}
              fallback={
                <TopAppBarAction
                  type="button"
                  icon="bookmark"
                  filledIcon
                  onClick={openBookmarkDialog}
                />
              }
            >
              <TopAppBarAction
                type="button"
                aria-label="更新到站資訊"
                icon="refresh"
                onClick={estimateTimes.refetch}
              />
              <TopAppBarAction
                type="button"
                aria-label="更多選項"
                aria-expanded={isMenuOpen()}
                aria-haspopup="menu"
                icon="more_vert"
                ref={setMenuAnchorEl}
                onClick={[setMenuOpen, !isMenuOpen()]}
              />
            </Show>
          </TopAppBarActions>
          <Menu
            id="more-menu"
            positioning="popover"
            anchor={menuAnchorEl()}
            open={isMenuOpen()}
            on:closed={() => setMenuOpen(false)}
          >
            <MenuItem href="categories">
              <ItemStart as={Icon} name="category" />
              <ItemHeadline>管理分類</ItemHeadline>
            </MenuItem>
            <MenuItem href="preferences">
              <ItemStart as={Icon} name="settings" />
              <ItemHeadline>偏好設定</ItemHeadline>
            </MenuItem>
          </Menu>
        </TopAppBar>
        <Show when={Object.keys(categories()).length > 0}>
          <Tabs aria-label="收藏分類">
            <Entries of={categories()}>
              {(id, name) => (
                <Tab
                  id={`category-${id}-tab`}
                  aria-controls={`category-${id}-panel`}
                  variant="primary"
                  active={id === selectedCategoryId()}
                  onClick={[onSelectCategory, id]}
                >
                  {name()}
                </Tab>
              )}
            </Entries>
          </Tabs>
        </Show>
      </LayoutHeader>
      <LayoutContent
        class={styles['content']}
        ref={(el) => {
          containerEl = el
        }}
      >
        <For
          each={Object.keys(categories())}
          fallback={
            <section class={styles['fallback']}>
              <h1>尚無收藏站牌</h1>
              <p>您可點擊右下方的按鈕搜尋公車路線，並將常用的站牌加入收藏。</p>
            </section>
          }
        >
          {(categoryId) => (
            <div
              id={`category-${categoryId}-panel`}
              aria-labelledby={`category-${categoryId}-tab`}
              role="tabpanel"
              hidden={categoryId !== selectedCategoryId()}
            >
              <SortableList
                container={containerEl}
                onDropped={(from, to) => {
                  bookmarkService.reorderBookmark(categoryId, from, to)
                }}
              >
                <Key each={bookmarks.data().get(categoryId)?.bookmarks ?? []} by="stopId">
                  {(bookmark, index) => (
                    <SortableListItem
                      href={`routes/${bookmark().routeId}#direction-${bookmark().direction}-${bookmark().stopId}`}
                      class={classNames(
                        selectedStopIds.has(bookmark().stopId) && styles['selected'],
                      )}
                      onClick={[onClickBookmark, bookmark().stopId]}
                      onPointerCancel={onEndPressBookmark}
                      onPointerDown={[onStartPressBookmark, bookmark().stopId]}
                      onPointerLeave={onEndPressBookmark}
                      onPointerUp={onEndPressBookmark}
                    >
                      <ItemStart
                        as={BusArrivalTime}
                        value={estimateTimes.data().get(bookmark().stopId)}
                      />
                      <ItemHeadline>{bookmark().routeName}</ItemHeadline>
                      <ItemSupportingText>{`${bookmark().stopName} ‣ 往${bookmark().destinationStopName}`}</ItemSupportingText>
                      <ItemEnd as={SortableListDragHandle} index={index()} />
                    </SortableListItem>
                  )}
                </Key>
              </SortableList>
            </div>
          )}
        </For>
        <Fab
          aria-label="搜尋公車路線"
          variant="primary"
          icon="search"
          class={styles['fab']}
          onClick={[navigate, '/routes']}
        />
        <BookmarkDialog
          id="bookmark-dialog"
          open={categoryStatus() !== undefined}
          categories={categoryStatus() ?? {}}
          onClose={onCloseBookmarkDialog}
        />
        <Snackbar
          open={snackbarText() !== undefined}
          class={styles['snackbar']}
          onClosed={popSnackbarText}
        >
          <SnackbarContent>{snackbarText()}</SnackbarContent>
        </Snackbar>
      </LayoutContent>
    </Layout>
  )
}
