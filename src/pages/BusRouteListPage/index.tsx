import { createAutofocus } from '@solid-primitives/autofocus'
import { useNavigate, useSearchParams } from '@solidjs/router'
import type { JSX } from 'solid-js'
import { createSignal, For, Show } from 'solid-js'

import { IconButton } from '@/components/IconButton'
import { Layout, LayoutContent, LayoutHeader } from '@/components/Layout'
import { List, ListItem } from '@/components/List'
import { useBusInfoService } from '@/services'
import type { JSXEvent } from '@/utils/events'

import styles from './style.module.scss'

function normalizeRouteName(name: string): string {
  return name.toUpperCase()
}

export default function BusRouteListPage(): JSX.Element {
  const [searchParams, setSearchParams] = useSearchParams<{ readonly q?: string }>()
  const [filter, setFilter] = createSignal(searchParams.q ?? '')
  const [isComposition, setComposition] = createSignal(false)
  function onFilterInput(event: JSXEvent<HTMLInputElement>): void {
    setFilter(normalizeRouteName(event.currentTarget.value))
    if (!isComposition()) {
      setSearchParams({ q: filter() }, { replace: true })
    }
  }

  function onClearFilter(): void {
    setFilter('')
    setSearchParams({ q: null }, { replace: true })
    searchInputEl.focus()
  }

  function onFilterCompositionStart(): void {
    setComposition(true)
  }

  function onFilterCompositionEnd(): void {
    setSearchParams({ q: filter() }, { replace: true })
    setComposition(false)
  }

  const busInfoService = useBusInfoService()
  const routes = busInfoService.getRoutes()

  let searchInputEl: HTMLInputElement
  createAutofocus(() => searchInputEl)

  const navigate = useNavigate()
  return (
    <Layout>
      <LayoutHeader title="搜尋公車路線">
        <div class={styles['search-bar']}>
          <IconButton
            type="button"
            aria-label="返回"
            icon="arrow_back"
            class={styles['search-control']}
            onClick={[navigate, -1]}
          />
          <input
            type="search"
            name="q"
            value={filter()}
            aria-label="搜尋公車路線"
            placeholder="搜尋公車路線"
            class={styles['search-input']}
            ref={(el) => {
              searchInputEl = el
            }}
            onInput={onFilterInput}
            onCompositionStart={onFilterCompositionStart}
            onCompositionEnd={onFilterCompositionEnd}
          />
          <Show when={filter() !== ''}>
            <IconButton
              type="button"
              aria-label="清除搜尋字串"
              icon="clear"
              class={styles['search-control']}
              onClick={onClearFilter}
            />
          </Show>
        </div>
      </LayoutHeader>
      <LayoutContent>
        <List class={styles['content']}>
          <For each={routes.data()}>
            {(route) => (
              <Show
                when={
                  searchParams.q !== undefined &&
                  searchParams.q !== '' &&
                  normalizeRouteName(route.name).includes(searchParams.q)
                }
              >
                <ListItem href={`routes/${route.id}`}>{route.name}</ListItem>
              </Show>
            )}
          </For>
        </List>
      </LayoutContent>
    </Layout>
  )
}
