import { createAssert } from 'typia'
import { v4 as uuid } from 'uuid'

import type { v240114 } from '../../../interfaces/preference-service'
import * as v1 from './v1'

export const version = '240114'

export const defaultPreference: v240114.Preference = {
  theme: 'system',
  fontSize: 'md',
  bookmarks: {},
}

const assertPreference = createAssert<Partial<v240114.Preference>>()

export function parse(data: unknown, _version: string): v240114.Preference {
  if (_version === version) {
    const preferences = assertPreference(data)
    return {
      ...defaultPreference,

      // only remain known preferences
      ...Object.fromEntries(
        Object.entries(preferences).filter(([name]) => name in defaultPreference),
      ),
    }
  } else {
    const legacyPreferences = v1.parse(data, _version)
    const bookmarks: Record<string, v240114.BusStopCategoryData> = {}
    for (const [category, stopIds] of Object.entries(legacyPreferences.bookmarkData)) {
      let id: string
      do {
        id = uuid()
      } while (id in bookmarks)
      bookmarks[id] = {
        name: category,
        stopIds,
      }
    }

    return {
      ...defaultPreference,
      theme: legacyPreferences.theme,
      bookmarks,
    }
  }
}
