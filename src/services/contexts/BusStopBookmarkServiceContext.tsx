import type { ContextProviderProps } from '@solid-primitives/context'
import { createContextProvider } from '@solid-primitives/context'

import { ReactiveBusStopBookmarkService } from '../implementations/bus-stop-bookmark-service'
import type { BusStopBookmarkService } from '../interfaces/bus-stop-bookmark-service'
import { useBusInfoService } from './BusInfoServiceContext'
import { usePreferenceService } from './PreferenceServiceContext'

export interface BusStopBookmarkServiceContext extends BusStopBookmarkService {}

export interface BusStopBookmarkServiceProviderProps extends ContextProviderProps {}

// eslint-disable-next-line @typescript-eslint/naming-convention
const [BusStopBookmarkServiceProvider, _useBusStopBookmarkService] = createContextProvider<
  BusStopBookmarkServiceContext,
  BusStopBookmarkServiceProviderProps
>(() => {
  const busInfoService = useBusInfoService()
  const preferenceService = usePreferenceService()
  return new ReactiveBusStopBookmarkService(preferenceService, busInfoService)
})

function useBusStopBookmarkService(): BusStopBookmarkServiceContext {
  const context = _useBusStopBookmarkService()
  if (context === undefined) {
    throw new Error('BusStopBookmarkService is not provided')
  }

  return context
}

export { BusStopBookmarkServiceProvider, useBusStopBookmarkService }
