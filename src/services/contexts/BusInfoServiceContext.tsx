import type { ContextProviderProps } from '@solid-primitives/context'
import { createContextProvider } from '@solid-primitives/context'

import { DataTaipeiBusInfoService } from '../implementations/bus-info-service/data-taipei'
import type { BusInfoService } from '../interfaces/bus-info-service'

export interface BusInfoServiceContext extends BusInfoService {}

export interface BusInfoServiceProviderProps extends ContextProviderProps {}

// eslint-disable-next-line @typescript-eslint/naming-convention
const [BusInfoServiceProvider, _useBusInfoService] = createContextProvider<
  BusInfoServiceContext,
  BusInfoServiceProviderProps
>(() => new DataTaipeiBusInfoService())

function useBusInfoService(): BusInfoServiceContext {
  const context = _useBusInfoService()
  if (context === undefined) {
    throw new Error('BusInfoService is not provided')
  }

  return context
}

export { BusInfoServiceProvider, useBusInfoService }
