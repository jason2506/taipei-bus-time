import type { AsyncData } from './async-data'
import type { BusDirection } from './bus-info-service'

export interface BusStopBookmark {
  readonly stopId: string
  readonly stopName: string
  readonly direction: BusDirection
  readonly routeId: string
  readonly routeName: string
  readonly destinationStopName: string
}

export interface BusStopCategory {
  readonly name: string
  readonly bookmarks: readonly BusStopBookmark[]
}

export interface BusStopBookmarkService {
  readonly hasBookmark: (stopId: string) => boolean
  readonly getStopIds: () => ReadonlySet<string>
  readonly getBookmarks: () => AsyncData<ReadonlyMap<string, BusStopCategory>>
  readonly getCategories: () => Readonly<Record<string, string>>
  readonly getCategoriesByStop: (stopId: string) => readonly string[]
  readonly renameCategory: (categoryId: string, name: string) => void
  readonly reorderCategory: (from: number, to: number) => void
  readonly reorderBookmark: (categoryId: string, from: number, to: number) => void
  readonly updateBookmarks: (
    stopIds: readonly string[],
    updatedCategories: Readonly<Record<string, boolean>>,
    newCategories: readonly string[],
  ) => void
}
