import type { Preference } from './versions'

export interface PreferenceService {
  readonly get: <P extends keyof Preference>(name: P) => Preference[P]
  readonly set: <P extends keyof Preference>(name: P, value: Preference[P]) => void
  readonly export: () => string
  readonly import: (json: string) => void
}

export * from './versions'
