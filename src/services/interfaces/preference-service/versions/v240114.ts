export type Theme = 'system' | 'dark' | 'light'

export type FontSize = 'md' | 'lg' | 'xl' | 'xxl'

export interface BusStopCategoryData {
  readonly name: string
  readonly stopIds: readonly string[]
}

export interface BusStopBookmarkData extends Readonly<Record<string, BusStopCategoryData>> {}

export interface Preference {
  readonly theme: Theme
  readonly fontSize: FontSize
  readonly bookmarks: BusStopBookmarkData
}
