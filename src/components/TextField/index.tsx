import '@material/web/textfield/outlined-text-field'

import type { TextField as _TextField } from '@material/web/textfield/internal/text-field'
import type { MdOutlinedTextField } from '@material/web/textfield/outlined-text-field'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'
import type { DynamicProps } from 'solid-js/web'
import { Dynamic } from 'solid-js/web'

import type { CommonAttributes, CustomElementAttributes } from '@/utils/attributes'

type MdTextFieldPropertyNames = 'placeholder' | 'type' | 'value'

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-outlined-text-field': CustomElementAttributes<
        MdOutlinedTextField,
        MdTextFieldPropertyNames
      >
    }
  }
}

export type TextFieldVariant = 'outlined'

export type TextFieldType = _TextField['type']

type MdTextFieldTagName<T extends TextFieldVariant = TextFieldVariant> = `md-${T}-text-field`

export interface TextFieldProps<T extends TextFieldVariant>
  extends CommonAttributes<HTMLElementTagNameMap[MdTextFieldTagName<T>]> {
  readonly placeholder?: string | undefined
  readonly type?: TextFieldType | undefined
  readonly value?: string
  readonly variant: T
}

export function TextField<T extends TextFieldVariant>(props: TextFieldProps<T>): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['placeholder', 'type', 'value', 'variant'])
  return (
    <Dynamic
      {...(restProps as DynamicProps<MdTextFieldTagName>)}
      component={`md-${localProps.variant}-text-field` as MdTextFieldTagName}
      prop:placeholder={localProps.placeholder}
      prop:type={localProps.type}
      prop:value={localProps.value}
    />
  )
}
