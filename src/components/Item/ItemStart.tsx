import type { JSX, ValidComponent } from 'solid-js'
import { splitProps } from 'solid-js'
import type { DynamicProps } from 'solid-js/web'
import { Dynamic } from 'solid-js/web'

const defaultComponent = 'span'

export type ItemStartProps<T extends ValidComponent = typeof defaultComponent> = Omit<
  DynamicProps<T>,
  'component' | 'slot'
> & {
  readonly as?: T | undefined
}

export function ItemStart<T extends ValidComponent = typeof defaultComponent>(
  props: ItemStartProps<T>,
): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['as'])
  return <Dynamic {...restProps} component={localProps.as ?? defaultComponent} slot="start" />
}
