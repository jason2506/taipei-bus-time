import type { JSX, ValidComponent } from 'solid-js'
import { splitProps } from 'solid-js'
import type { DynamicProps } from 'solid-js/web'
import { Dynamic } from 'solid-js/web'

const defaultComponent = 'span'

export type ItemHeadlineProps<T extends ValidComponent = typeof defaultComponent> = Omit<
  DynamicProps<T>,
  'component' | 'slot'
> & {
  readonly as?: T | undefined
}

export function ItemHeadline<T extends ValidComponent = typeof defaultComponent>(
  props: ItemHeadlineProps<T>,
): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['as'])
  return <Dynamic {...restProps} component={localProps.as ?? defaultComponent} slot="headline" />
}
