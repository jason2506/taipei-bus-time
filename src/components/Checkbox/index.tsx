import '@material/web/checkbox/checkbox'

import type { MdCheckbox } from '@material/web/checkbox/checkbox'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import type { CommonAttributes, CustomElementAttributes } from '@/utils/attributes'

type MdCheckboxPropertyNames = 'checked' | 'disabled' | 'indeterminate' | 'value'

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-checkbox': CustomElementAttributes<MdCheckbox, MdCheckboxPropertyNames>
    }
  }
}

export interface CheckboxProps extends CommonAttributes<MdCheckbox> {
  readonly indeterminate?: boolean | undefined
  readonly checked?: boolean | undefined
  readonly disabled?: boolean | undefined
  readonly value?: string | undefined
}

export function Checkbox(props: CheckboxProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, [
    'indeterminate',
    'checked',
    'disabled',
    'value',
  ])
  return (
    <md-checkbox
      {...restProps}
      attr:touch-target="wrapper"
      prop:checked={localProps.checked}
      prop:disabled={localProps.disabled}
      prop:indeterminate={localProps.indeterminate}
      prop:value={localProps.value}
    />
  )
}
