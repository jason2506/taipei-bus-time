import type { JSX } from 'solid-js'
import { createMemo } from 'solid-js'

import styles from './style.module.scss'

export interface BusArrivalTimeProps {
  readonly slot?: string
  readonly value: number | undefined
}

export function BusArrivalTime(props: BusArrivalTimeProps): JSX.Element {
  const color = createMemo(() =>
    props.value === undefined || props.value < 0
      ? 'inactive'
      : props.value < 240
        ? 'highlight'
        : 'normal',
  )
  return (
    <span
      class={styles[color()]}
      slot={
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        props.slot!
      }
    >
      {props.value === undefined || props.value < -4 ? (
        '-'
      ) : props.value === -1 ? (
        <>
          尚未
          <br />
          發車
        </>
      ) : props.value === -2 ? (
        <>
          交管
          <br />
          不停靠
        </>
      ) : props.value === -3 ? (
        <>
          末班車
          <br />
          已過
        </>
      ) : props.value === -4 ? (
        <>
          今日
          <br />
          未營運
        </>
      ) : props.value < 60 ? (
        '< 1 分'
      ) : (
        `${Math.floor(props.value / 60)} 分`
      )}
    </span>
  )
}
