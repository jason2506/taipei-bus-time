import '@material/web/progress/linear-progress'

import type { MdLinearProgress } from '@material/web/progress/linear-progress'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import type { CommonAttributes, CustomElementAttributes } from '@/utils/attributes'

type MdLinearProgressPropertyNames = 'indeterminate'

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-linear-progress': CustomElementAttributes<MdLinearProgress, MdLinearProgressPropertyNames>
    }
  }
}

export interface LinearProgressProps extends CommonAttributes<MdLinearProgress> {
  readonly indeterminate?: boolean | undefined
}

export function LinearProgress(props: LinearProgressProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['indeterminate'])
  return <md-linear-progress {...restProps} prop:indeterminate={localProps.indeterminate} />
}
