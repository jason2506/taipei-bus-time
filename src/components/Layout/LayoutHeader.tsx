import classNames from 'classnames'
import type { JSX } from 'solid-js'
import { createEffect, Show } from 'solid-js'

import { LinearProgress } from '../LinearProgress'
import styles from './style.module.scss'

export interface LayoutHeaderProps {
  readonly title?: string | undefined
  readonly loadingInfo?: string | undefined
  readonly children: JSX.Element
}

const appTitle = '臺北公車動態'

export function LayoutHeader(props: LayoutHeaderProps): JSX.Element {
  createEffect(() => {
    const title = props.title
    document.title = title === undefined ? appTitle : `${title} | ${appTitle}`
  })
  return (
    <header class={classNames(styles['header'])}>
      {props.children}
      <Show when={props.loadingInfo}>
        <LinearProgress
          class={styles['progress']}
          indeterminate
          aria-label={
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            props.loadingInfo!
          }
        />
      </Show>
    </header>
  )
}
