import type { Ref } from '@solid-primitives/refs'
import classNames from 'classnames'
import type { JSX } from 'solid-js'

import styles from './style.module.scss'

export interface LayoutContentProps {
  readonly class?: string | undefined
  readonly children: JSX.Element
  readonly ref?: Ref<HTMLElement> | undefined
}

export function LayoutContent(props: LayoutContentProps): JSX.Element {
  return (
    <main
      tabindex="-1"
      ref={
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        props.ref!
      }
      class={classNames(props.class, styles['content'])}
    >
      {props.children}
    </main>
  )
}
