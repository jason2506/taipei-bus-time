import type { ContextProviderProps } from '@solid-primitives/context'
import { createContextProvider } from '@solid-primitives/context'

export interface SnackbarContext {
  readonly close: () => void
}

export interface SnackbarContextProviderProps extends SnackbarContext, ContextProviderProps {}

// eslint-disable-next-line @typescript-eslint/naming-convention
const [SnackbarContextProvider, _useSnackbarContext] = createContextProvider<
  SnackbarContext,
  SnackbarContextProviderProps
>((props: SnackbarContextProviderProps) => props)

function useSnackbarContext(): SnackbarContext {
  const context = _useSnackbarContext()
  if (context === undefined) {
    throw new Error('SnackbarContext is not provided')
  }

  return context
}

export { SnackbarContextProvider, useSnackbarContext }
