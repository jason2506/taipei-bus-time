import '@material/web/tabs/tabs'

import type { MdTabs } from '@material/web/tabs/tabs'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import type { CommonAttributes, CustomElementAttributes } from '@/utils/attributes'

type MdTabsPropertyNames = 'autoActivate'

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-tabs': CustomElementAttributes<MdTabs, MdTabsPropertyNames>
    }
  }
}

export interface TabsProps extends CommonAttributes<MdTabs> {
  readonly autoActivate?: boolean | undefined
}

export function Tabs(props: TabsProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['autoActivate'])
  return <md-tabs {...restProps} prop:autoActivate={localProps.autoActivate} />
}
