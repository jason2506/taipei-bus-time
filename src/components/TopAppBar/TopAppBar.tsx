import classNames from 'classnames'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import type { CommonAttributes } from '@/utils/attributes'

import styles from './style.module.scss'

export interface TopAppBarProps extends CommonAttributes<HTMLDivElement> {}

export function TopAppBar(props: TopAppBarProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['class'])
  return <div {...restProps} class={classNames(styles['container'], localProps.class)} />
}
