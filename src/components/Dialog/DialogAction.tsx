import type { JSX } from 'solid-js'

import type { ButtonProps } from '../Button'
import { Button } from '../Button'
import { useDialogContext } from './DialogContext'

export type DialogActionProps = Omit<ButtonProps<'text'>, 'variant'>

export function DialogAction(props: DialogActionProps): JSX.Element {
  const { form } = useDialogContext()
  return <Button {...props} form={form()?.id} variant="text" />
}
