import '@material/web/dialog/dialog'

import type { MdDialog } from '@material/web/dialog/dialog'
import type { JSX } from 'solid-js'
import { splitProps } from 'solid-js'

import type { CommonAttributes, CustomElementAttributes, OnAttributes } from '@/utils/attributes'

import { DialogContextProvider } from './DialogContext'

type MdDialogPropertyNames = 'open' | 'returnValue' | 'type'

// eslint-disable-next-line @typescript-eslint/consistent-type-definitions
type MdDialogEvents = {
  readonly closed: Event
}

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-dialog': CustomElementAttributes<MdDialog, MdDialogPropertyNames, MdDialogEvents>
    }
  }
}

export type DialogType = MdDialog['type']

export interface DialogProps
  extends CommonAttributes<MdDialog>,
    OnAttributes<MdDialog, MdDialogEvents> {
  readonly open?: boolean | undefined
  readonly returnValue?: string
  readonly type?: DialogType
}

export function Dialog(props: DialogProps): JSX.Element {
  const [localProps, restProps] = splitProps(props, ['open', 'returnValue', 'type'])
  return (
    <DialogContextProvider>
      <md-dialog
        {...restProps}
        prop:open={localProps.open}
        prop:returnValue={localProps.returnValue}
        prop:type={localProps.type}
      />
    </DialogContextProvider>
  )
}
