import '@material/web/divider/divider'

import type { MdDivider } from '@material/web/divider/divider'
import type { JSX } from 'solid-js'

import type { CommonAttributes, CustomElementAttributes } from '@/utils/attributes'

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-divider': CustomElementAttributes<MdDivider>
    }
  }
}

export interface DividerProps extends CommonAttributes<MdDivider> {}

export function Divider(props: DividerProps): JSX.Element {
  return <md-divider {...props} />
}
