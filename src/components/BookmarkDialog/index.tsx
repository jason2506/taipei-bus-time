import type { MdCheckbox } from '@material/web/checkbox/checkbox'
import type { MdDialog } from '@material/web/dialog/dialog'
import type { MdOutlinedTextField } from '@material/web/textfield/outlined-text-field'
import { Entries } from '@solid-primitives/keyed'
import type { Accessor, JSX } from 'solid-js'
import { createSignal, For } from 'solid-js'

import type { JSXEvent } from '@/utils/events'

import {
  Dialog,
  DialogAction,
  DialogActions,
  DialogContent,
  DialogField,
  DialogHeadline,
} from '../Dialog'
import { IconButton } from '../IconButton'
import { TextField } from '../TextField'
import styles from './style.module.scss'

export interface BookmarkDialogProps {
  readonly id: string
  readonly categories: Readonly<Record<string, readonly [string, boolean | null]>>
  readonly onClose: (categories?: {
    readonly updated: Readonly<Record<string, boolean>>
    readonly created: readonly string[]
  }) => void
  readonly open?: boolean | undefined
}

export function BookmarkDialog(props: BookmarkDialogProps): JSX.Element {
  const [newCategory, setNewCategory] = createSignal('')
  const [newCategories, setNewCategories] = createSignal<readonly string[]>([])
  const [updatedCategories, setUpdatedCategories] = createSignal<Readonly<Record<string, boolean>>>(
    {},
  )

  function updateCategory(id: string, event: JSXEvent<MdCheckbox>): void {
    setUpdatedCategories({
      ...updatedCategories(),
      [id]: event.currentTarget.checked,
    })
  }

  let dialogEl!: MdDialog
  function onCloseDialog(): void {
    if (dialogEl.returnValue === 'submit') {
      const category = newCategory()
      props.onClose({
        updated: updatedCategories(),
        created: category === '' ? newCategories() : [...newCategories(), category],
      })
    } else {
      props.onClose()
    }

    dialogEl.returnValue = ''
    setNewCategory('')
    setNewCategories([])
    setUpdatedCategories({})
  }

  function onRemoveCategory(index: Accessor<number>): void {
    const result = [...newCategories()]
    result.splice(index(), 1)
    setNewCategories(result)
  }

  function onChangeNewCategory(event: JSXEvent<MdOutlinedTextField>): void {
    setNewCategory(event.currentTarget.value)
  }

  function onKeyPressNewCategory(event: JSXEvent<MdOutlinedTextField, KeyboardEvent>): void {
    if (event.key === 'Enter') {
      onCreateCategory()
    }
  }

  let inputEl: MdOutlinedTextField
  function onCreateCategory(): void {
    const category = newCategory()
    if (category !== '') {
      setNewCategories([...newCategories(), category])
      setNewCategory('')
    }

    inputEl.focus()
  }

  return (
    <Dialog
      id={props.id}
      ref={(el) => {
        dialogEl = el
      }}
      open={props.open}
      on:closed={onCloseDialog}
    >
      <DialogHeadline>收藏站牌</DialogHeadline>
      <DialogContent>
        <Entries of={props.categories}>
          {(id, category) => (
            <DialogField
              type="checkbox"
              label={category()[0]}
              checked={category()[1] ?? false}
              indeterminate={category()[1] === null}
              onChange={[updateCategory, id]}
            />
          )}
        </Entries>
        <For each={newCategories()}>
          {(name, index) => (
            <DialogField type="checkbox" label={name} checked disabled>
              <IconButton
                type="button"
                aria-label="刪除收藏分類"
                icon="delete"
                class={styles['remove-category-button']}
                onClick={[onRemoveCategory, index]}
              />
            </DialogField>
          )}
        </For>
        <TextField
          ref={(el) => {
            inputEl = el
          }}
          aria-label="收藏分類名稱"
          variant="outlined"
          value={newCategory()}
          placeholder="新增收藏分類"
          class={styles['new-category-input']}
          onInput={onChangeNewCategory}
          onKeyPress={onKeyPressNewCategory}
        >
          <IconButton
            type="button"
            aria-label="新增收藏分類"
            disabled={newCategory() === ''}
            icon="add_box"
            slot="leading-icon"
            onClick={onCreateCategory}
          />
        </TextField>
      </DialogContent>
      <DialogActions>
        <DialogAction>取消</DialogAction>
        <DialogAction value="submit">確定</DialogAction>
      </DialogActions>
    </Dialog>
  )
}
