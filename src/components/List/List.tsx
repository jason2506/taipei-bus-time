import '@material/web/list/list'

import type { MdList } from '@material/web/list/list'
import type { JSX } from 'solid-js'

import type { CommonAttributes, CustomElementAttributes } from '@/utils/attributes'

declare module 'solid-js' {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace JSX {
    interface IntrinsicElements {
      // eslint-disable-next-line @typescript-eslint/naming-convention
      'md-list': CustomElementAttributes<MdList>
    }
  }
}

export interface ListProps extends CommonAttributes<MdList> {}

export function List(props: ListProps): JSX.Element {
  return <md-list {...props} />
}
