import type { JSX } from 'solid-js'

type PickOptional<T, TPropertyNames extends keyof T & string> = {
  [TName in TPropertyNames as `prop:${TName}`]?: T[TName] | undefined
}

export type CommonAttributes<T, TPropertyNames extends keyof JSX.HTMLAttributes<T> = never> = Pick<
  JSX.HTMLAttributes<T>,
  'class' | 'children' | 'id' | 'ref' | 'slot' | TPropertyNames
> &
  JSX.CustomEventHandlersCamelCase<T>

export type OnAttributes<T, TEvents extends Record<string, Event>> = {
  readonly [TName in keyof TEvents as `on:${TName & string}`]?:
    | JSX.EventHandlerUnion<T, TEvents[TName]>
    | undefined
}

export type CustomElementAttributes<
  T extends HTMLElement,
  TPropertyNames extends keyof T & string = never,
  TEvents extends Record<string, Event> = Record<string, Event>,
> = PickOptional<T, TPropertyNames> & CommonAttributes<T> & OnAttributes<T, TEvents>

export function excludeBooleanValue<T>(
  value: T | 'true' | 'false' | boolean,
): T | 'true' | 'false' {
  return typeof value === 'boolean' ? `${value}` : value
}
