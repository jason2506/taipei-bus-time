export type JSXEvent<TTarget, TEvent extends Event = Event> = TEvent & {
  readonly currentTarget: TTarget
  readonly target: Element
}

export function preventDefaultEvent(event: Event): void {
  event.stopPropagation()
  event.preventDefault()
}
