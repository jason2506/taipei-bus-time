import type { Accessor } from 'solid-js'
import { createEffect, createSignal } from 'solid-js'

export interface QueueActions<T> {
  readonly push: (item: T) => void
  readonly pop: () => void
}

export type Queue<T> = readonly [Accessor<T | undefined>, QueueActions<T>]

export function createQueue<T>(): Queue<T> {
  const [head, setHead] = createSignal<T>()
  const queue: T[] = []

  function push(item: T): void {
    if (head() === undefined) {
      setHead(() => item)
    } else {
      queue.push(item)
    }
  }

  createEffect(() => {
    if (head() === undefined) {
      setHead(() => queue.shift())
    }
  })

  return [
    head,
    {
      push: (item) => {
        queueMicrotask(push.bind(null, item))
      },
      pop: () => {
        setHead()
      },
    },
  ]
}
