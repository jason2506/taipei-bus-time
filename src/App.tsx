import './style.scss'

import { useBeforeLeave } from '@solidjs/router'
import type { JSX } from 'solid-js'

import { ThemeProvider } from './components/ThemeProvider'
import {
  BusInfoServiceProvider,
  BusStopBookmarkServiceProvider,
  PreferenceServiceProvider,
} from './services'

export interface AppProps {
  readonly children?: JSX.Element
}

export function App(props: AppProps): JSX.Element {
  useBeforeLeave((event) => {
    if (event.options?.replace !== true) {
      document.querySelector<HTMLElement>('#root')?.focus()
    }
  })
  return (
    <PreferenceServiceProvider>
      <BusInfoServiceProvider>
        <BusStopBookmarkServiceProvider>
          <ThemeProvider>{props.children}</ThemeProvider>
        </BusStopBookmarkServiceProvider>
      </BusInfoServiceProvider>
    </PreferenceServiceProvider>
  )
}
