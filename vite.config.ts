// eslint-disable-next-line n/no-sync
import { execSync } from 'node:child_process'
import path from 'node:path'

import typescript from '@rollup/plugin-typescript'
import autoprefixer from 'autoprefixer'
import type { Plugin } from 'vite'
import { defineConfig } from 'vite'
import { VitePWA as pwa } from 'vite-plugin-pwa'
import solid from 'vite-plugin-solid'

// eslint-disable-next-line n/no-sync
const commitHash = execSync('git rev-parse --short HEAD').toString()

export default defineConfig({
  define: {
    'import.meta.env.COMMIT_HASH': JSON.stringify(commitHash),
  },
  plugins: [
    typescript() as unknown as Plugin,
    solid(),
    pwa({
      registerType: 'autoUpdate',
      srcDir: 'src',
      filename: 'sw.ts',
      strategies: 'injectManifest',
      manifest: {
        name: '臺北公車動態',
        short_name: '臺北公車動態',
        display: 'standalone',
        start_url: '.',
        icons: [
          {
            src: 'images/icon-48.png',
            sizes: '48x48',
            type: 'image/png',
          },
          {
            src: 'images/icon-72.png',
            sizes: '72x72',
            type: 'image/png',
          },
          {
            src: 'images/icon-96.png',
            sizes: '96x96',
            type: 'image/png',
          },
          {
            src: 'images/icon-144.png',
            sizes: '144x144',
            type: 'image/png',
          },
          {
            src: 'images/icon-168.png',
            sizes: '168x168',
            type: 'image/png',
          },
          {
            src: 'images/icon-192.png',
            sizes: '192x192',
            type: 'image/png',
          },
        ],
      },
    }),
  ],
  css: {
    postcss: {
      plugins: [autoprefixer()],
    },
  },
  resolve: {
    alias: [
      {
        find: '@',
        replacement: path.resolve(__dirname, 'src'),
      },
    ],
  },
})
